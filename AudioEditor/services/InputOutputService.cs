namespace Services {

    /// <summary>
    /// Service managing inputs and outputs.
    /// </summary>
    public static class InputOutputService {
        
        private static readonly string OUT_DIR = @"out/";

        /// <summary>
        /// Write a file to the "out" directory.
        /// </summary>
        /// <param name="filename">The file name</param>
        /// <param name="bytes">The bytes to write</param>
        public static void WriteFile(string filename, byte[] bytes) {

            Console.WriteLine("Writing file {0} to disk", filename);

            // TODO : test the method

            var destPath = string.Concat(OUT_DIR, filename);

            if (File.Exists(destPath))
            {
                File.Delete(destPath);
                // Create a file to write to
                
            } 

            File.WriteAllBytes(destPath, bytes);
        }
    }
}