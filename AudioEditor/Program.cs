﻿using Factories;
using Models;
using Visitors;

class Program
{
    static void Main(string[] args)
    {
        if(args.Length == 0) {
            Console.WriteLine("No argument provided. Please provide a suitable stereo file to extract the channels from.");
            return;
        }

        //Initialize Factories
        var wavStereoFileFactory = new WavStereoFileFactory();
        var pcmStereoFileFactory = new PcmStereoFileFactory();

        // Initialize Visitors
        var channelExtractorVisitor = new ChannelExtractorVisitor(wavStereoFileFactory, pcmStereoFileFactory);

        foreach (var filePath in args)
        {
            IStereoFile stereoFile;
            var fileType = Path.GetExtension(filePath).ToLower();

            switch (fileType)
            {
                case ".wav":
                    stereoFile = wavStereoFileFactory.CreateStereoFile(filePath);
                    break;

                case ".pcm":
                    stereoFile = pcmStereoFileFactory.CreateStereoFile(filePath);
                    break;

                default:
                    Console.WriteLine("Unknown file format.");
                    return;
            }

            Console.WriteLine(stereoFile.ToString());
            
            stereoFile.AcceptVisitor(channelExtractorVisitor);
        }
    }
}