using Models;

namespace Factories {

    public class PcmStereoFileFactory : IStereoFileFactory<PcmStereoFile>
    {
        public PcmStereoFile CreateStereoFile(string filePath)
        {
            var file = new PcmStereoFile();

            if (!File.Exists(filePath))
            {
                return file;
            }

            throw new NotImplementedException();
        }

        public void WriteFileToDisk(PcmStereoFile file, string filename)
        {
            throw new NotImplementedException();
        }
    }
}