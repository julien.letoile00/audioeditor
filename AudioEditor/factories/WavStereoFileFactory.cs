using System.Text;
using Models;
using Services;

namespace Factories {

    public class WavStereoFileFactory : IStereoFileFactory<WavStereoFile> {

        public WavStereoFile CreateStereoFile(string filePath) {
            var file = new WavStereoFile();

            if (!File.Exists(filePath))
            {
                return file;
            }

            var audioBytes = File.ReadAllBytes(filePath);

            file.FilePath = filePath;
            file.RiffChunk = Encoding.ASCII.GetString(audioBytes, 0, 4);
            file.FileSize = BitConverter.ToInt32(audioBytes, 4);
            file.FileType = Encoding.ASCII.GetString(audioBytes, 8, 4);
            file.FormatChunkMarker = Encoding.ASCII.GetString(audioBytes, 12, 4);
            file.FormatDataLength = BitConverter.ToInt32(audioBytes, 16);
            file.FormatType = BitConverter.ToInt16(audioBytes, 20);
            file.ChannelCount = BitConverter.ToInt16(audioBytes, 22);
            file.SampleRate = BitConverter.ToInt32(audioBytes, 24);
            file.ByteRate = BitConverter.ToInt32(audioBytes, 28);
            file.BlockAlign = BitConverter.ToInt16(audioBytes, 32);
            file.BitsPerSample = BitConverter.ToInt16(audioBytes, 34);
            file.DataChunkHeader = Encoding.ASCII.GetString(audioBytes, 36, 4);
            file.DataSize = BitConverter.ToInt32(audioBytes, 40);
            file.HeaderSize = audioBytes.Length - file.DataSize;

            var data = new byte[file.DataSize];
            for (int i = file.HeaderSize; i < audioBytes.Length; i++)
            {
                data[i - file.HeaderSize] = audioBytes[i];
            }
            file.Data = data;

            return file;
        }

        public void WriteFileToDisk(WavStereoFile file, string filename) {

            var bytes = new List<byte>();

            bytes.AddRange(Encoding.ASCII.GetBytes(file.RiffChunk));
            bytes.AddRange(BitConverter.GetBytes(file.FileSize));
            bytes.AddRange(Encoding.ASCII.GetBytes(file.FileType));
            bytes.AddRange(Encoding.ASCII.GetBytes(file.FormatChunkMarker));
            bytes.AddRange(BitConverter.GetBytes(file.FormatDataLength));
            bytes.AddRange(BitConverter.GetBytes(file.FormatType));
            bytes.AddRange(BitConverter.GetBytes(file.ChannelCount));
            bytes.AddRange(BitConverter.GetBytes(file.SampleRate));
            bytes.AddRange(BitConverter.GetBytes(file.ByteRate));
            bytes.AddRange(BitConverter.GetBytes(file.BlockAlign));
            bytes.AddRange(BitConverter.GetBytes(file.BitsPerSample));
            bytes.AddRange(Encoding.ASCII.GetBytes(file.DataChunkHeader));
            bytes.AddRange(BitConverter.GetBytes(file.DataSize));
            bytes.AddRange(file.Data);

            InputOutputService.WriteFile(filename, [.. bytes]);
        }
    }
}