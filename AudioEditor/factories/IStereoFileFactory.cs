using Models;

namespace Factories {

    public interface IStereoFileFactory<FileType> where FileType : IStereoFile {

        FileType CreateStereoFile(string filePath);

        void WriteFileToDisk(FileType file, string filename);
        
    }
}