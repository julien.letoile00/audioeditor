using System.Text;
using Visitors;

namespace Models {

    /// <summary>
    /// Object representing a WAVE stereo file.
    /// </summary>
    public class WavStereoFile : IStereoFile {

        #region Constructor

        public WavStereoFile() {
            FilePath = "";
            RiffChunk = "";
            FileType = "";
            FormatChunkMarker = "";
            DataChunkHeader = "";
            Data = [];
        }

        #endregion Constructor

        #region Properties

        public string FilePath { get; set; }

        public string RiffChunk { get; set; }

        public int FileSize { get; set; }

        public string FileType { get; set; }

        public string FormatChunkMarker { get; set; }

        public int FormatDataLength { get; set; }

        public short FormatType { get; set; }

        public short ChannelCount { get; set; }

        public int SampleRate { get; set; }

        public int ByteRate { get; set; }

        public short BlockAlign { get; set; }

        public short BitsPerSample { get; set; }

        public string DataChunkHeader { get; set; }

        public int DataSize { get; set; }

        public int HeaderSize {get; set; }

        public byte[] Data { get; set; }

        #endregion Properties

        #region Public Methods

        public object Clone()
        {
            var stereoFile = (WavStereoFile) MemberwiseClone();
            return stereoFile;
        }

        public override string ToString() {
            var str = new StringBuilder("WavStereoFile");
            str.AppendLine(" {");
            str.Append("   FilePath : ").AppendLine(FilePath);
            str.Append("   RiffChunk : ").AppendLine(RiffChunk);
            str.Append("   FileSize : ").AppendLine(FileSize.ToString());
            str.Append("   FileType : ").AppendLine(FileType);
            str.Append("   FormatChunkMarker : ").AppendLine(FormatChunkMarker);
            str.Append("   FormatDataLength : ").AppendLine(FormatDataLength.ToString());
            str.Append("   FormatType : ").AppendLine(FormatType.ToString());
            str.Append("   ChannelCount : ").AppendLine(ChannelCount.ToString());
            str.Append("   SampleRate : ").AppendLine(SampleRate.ToString());
            str.Append("   ByteRate : ").AppendLine(ByteRate.ToString());
            str.Append("   BlockAlign : ").AppendLine(BlockAlign.ToString());
            str.Append("   BitsPerSample : ").AppendLine(BitsPerSample.ToString());
            str.Append("   DataChunkHeader : ").AppendLine(DataChunkHeader);
            str.Append("   DataSize : ").AppendLine(DataSize.ToString());
            str.Append("   HeaderSize : ").AppendLine(HeaderSize.ToString());
            str.Append("   Data array size : ").AppendLine(Data.Length.ToString());
            str.AppendLine("}");

            return str.ToString();
        }

        public void AcceptVisitor(IVisitor visitor) {
            if(string.IsNullOrWhiteSpace(FilePath)) {
                return;
            }

            visitor.Visit(this);
        }

        #endregion Public Methods

    }

}

