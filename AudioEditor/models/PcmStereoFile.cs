using Visitors;

namespace Models {

    public class PcmStereoFile : IStereoFile {

        #region Constructor

        public PcmStereoFile() {
            FilePath = "";
            FileType = "";
            Data = [];
            HeaderSize = 0;
            DataSize = 0;
        }

        #endregion Constructor

        #region Properties

        public string FilePath { get; set; }

        public string FileType { get; set; }

        public byte[] Data { get; set; }

        public int HeaderSize { get; set; }

        public int DataSize { get; set; }

        #endregion Properties

        #region Public Methods

        public object Clone()
        {
            var stereoFile = (PcmStereoFile) MemberwiseClone();
            return stereoFile;
        }

        public void AcceptVisitor(IVisitor visitor) {
            if(string.IsNullOrWhiteSpace(FilePath)) {
                return;
            }

            visitor.Visit(this);
        }

        #endregion Public Methods

    }

}

