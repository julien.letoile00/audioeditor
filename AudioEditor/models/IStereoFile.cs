using Visitors;

namespace Models {

    public interface IStereoFile : ICloneable {

        #region Properties

        /// <summary>
        /// Path of the stereo file.
        /// </summary>
        public string FilePath { get; }

        /// <summary>
        /// Type of the file as set in the header.
        /// </summary>
        public string FileType { get; set; }

        /// <summary>
        /// Size of the header section
        /// </summary>
        public int HeaderSize {get; set; }

        /// <summary>
        /// Size of the data section
        /// </summary>
        public int DataSize { get; set; }

        /// <summary>
        /// File data.
        /// </summary>
        public byte[] Data { get; set; }

        #endregion Properties

        /// <summary>
        /// Allows a visitor to perform its action over the stereo file.
        /// </summary>
        /// <param name="visitor">The visitor performing the action</param>
        void AcceptVisitor(IVisitor visitor);
    }

}
