using System.Text;
using Factories;
using Models;
using Services;

namespace Visitors {

    /// <summary>
    /// Visitor extracting both channels of a stereo file when visiting.
    /// </summary>
    public class ChannelExtractorVisitor : IVisitor {

        #region Attributes

        /// <summary>
        /// Factory for Wave files.
        /// </summary>
        private WavStereoFileFactory m_wavStereoFileFactory;

        /// <summary>
        /// Factory for PCM files.
        /// </summary>
        private PcmStereoFileFactory m_pcmStereoFileFactory;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Creates a visitor extracting both channels of a stereo file when visiting.
        /// </summary>
        /// <param name="wavStereoFileFactory">Factory for Wave files</param>
        /// <param name="pcmStereoFileFactory">Factory for PCM files</param>
        public ChannelExtractorVisitor(WavStereoFileFactory wavStereoFileFactory,
                                       PcmStereoFileFactory pcmStereoFileFactory) {
            m_wavStereoFileFactory = wavStereoFileFactory;
            m_pcmStereoFileFactory = pcmStereoFileFactory;
        }

        #endregion Constructor

        /// <summary>
        /// Extracts both channels of a PCM stereo file.
        /// </summary>
        /// <param name="file"></param>
        public void Visit(PcmStereoFile file) {
            if (File.Exists(file.FilePath))
            {
                Console.WriteLine("Extracting PCM file");

                // TODO
            }
        }

        /// <summary>
        /// Extracts both channels of a WAVE stereo file.
        /// </summary>
        /// <param name="file"></param>
        public void Visit(WavStereoFile file) {
            if (!File.Exists(file.FilePath))
            {
                return;
            }

            if(file.ChannelCount < 2) {
                return;
            }

            Console.WriteLine("Extracting WAV file");

            // Data storage for both channels
            var channelSize = file.DataSize / 2;
            var channel1Data = new byte[channelSize];
            var channel2Data = new byte[file.DataSize - channelSize];

            // Separate channels
            for (int i = 0; i < file.DataSize; i+=4)
            { 
                int j = i / 2;
                channel1Data[j] = file.Data[i];
                channel1Data[j+1] = file.Data[i+1];
                channel2Data[j] = file.Data[i+2];
                channel2Data[j+1] = file.Data[i+3];
            }

            // Create Wave files for each channel
            var channel1File = (WavStereoFile) file.Clone();
            channel1File.FileSize = file.FileSize - channelSize;
            channel1File.ChannelCount = 1;
            channel1File.DataSize = channelSize;
            channel1File.ByteRate = channel1File.SampleRate * channel1File.BitsPerSample * channel1File.ChannelCount / 8;
            channel1File.BlockAlign = (short) (channel1File.BitsPerSample * channel1File.ChannelCount / 8);
            channel1File.Data = channel1Data;

            var channel2File = (WavStereoFile) channel1File.Clone();
            channel2File.Data = channel2Data;

            Console.WriteLine("WAV files extracted : ");
            Console.WriteLine(channel1File.ToString());
            Console.WriteLine(channel2File.ToString());

            // Write new files to disk
            m_wavStereoFileFactory.WriteFileToDisk(channel1File, "Channel_1.wav");
            m_wavStereoFileFactory.WriteFileToDisk(channel2File, "Channel_2.wav");
        }

    }
}

