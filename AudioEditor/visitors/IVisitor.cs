using Models;

namespace Visitors {

    /// <summary>
    /// Visitor performing an action over an object, depending on its type.
    /// </summary>
    public interface IVisitor {

        /// <summary>
        /// Performs an action over a PcmStereoFile.
        /// </summary>
        /// <param name="file"></param>
        public void Visit(PcmStereoFile file);

        /// <summary>
        /// Performs an action over a WavStereoFile.
        /// </summary>
        /// <param name="file"></param>
        public void Visit(WavStereoFile file);
    }
}