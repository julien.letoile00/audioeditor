# AudioEditor

This project aims at applying different treatments on an audio file. All resulting files will be stored in the *out* directory. For now, the following operations can be performed :

* Extraction of both channels of a WAVE stereo file

## Getting started

To run the project, execute the following command :

```bash
dotnet run path\to\your\file.wav
```
